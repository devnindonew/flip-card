import 'package:flutter/material.dart';

class CardImage extends StatelessWidget {
  final String image;

  CardImage({this.image});
  
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(1.0),
      child: Image.asset(image),
    );
  }
}