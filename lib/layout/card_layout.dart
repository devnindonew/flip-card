import 'package:card_match/layout/card_image.dart';
import 'package:flutter/material.dart';

class CardLayout extends StatelessWidget {
  final Animation<double> animation;
  final String image;

  CardLayout({this.animation, this.image,});

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      child: CardImage(image: image),
      animation: animation,
      builder: (BuildContext context, Widget child) {
        final Matrix4 transform = new Matrix4.identity()
          ..scale(animation.value, 1.0, 1.0);
        return new Transform(
          transform: transform,
          alignment: FractionalOffset.center,
          child: child,
        );
      },
    );
  }
}