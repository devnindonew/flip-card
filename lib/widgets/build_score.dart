import 'package:flutter/material.dart';

class BuildScore extends StatelessWidget {
  final int score;
  final int time;

  BuildScore({this.score, this.time,});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 8.0, right: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            time.toString() + "s",
            style: TextStyle(
              fontSize: 32.0,
              color: Colors.white,
            ),
          ),
          Text(
            score.toString(),
            style: TextStyle(
              fontSize: 32.0,
              color: Colors.white,
            ),
          )
        ],
      ),
    );
  }
}