import 'package:card_match/widgets/board_view.dart';
import 'package:flutter/material.dart';

import '../card_board.dart';

class BuildBoard extends StatelessWidget {
  final Function() onWin;
  BuildBoard({this.onWin});
  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(8.0),
            child: CardBoard(onWin: onWin),
          ),
          BoardView(),
        ],
      ),
    );
  }
}
