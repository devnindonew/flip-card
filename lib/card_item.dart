import 'package:card_match/layout/card_image.dart';
import 'package:card_match/layout/card_layout.dart';
import 'package:card_match/model/card_model.dart';
import 'package:flutter/material.dart';

class CardItem extends StatefulWidget {
  final CardModel model;
  final Function(bool isOpened, int id) onFlipCard;

  CardItem({Key key, this.model, this.onFlipCard});
  @override
  _CardItemState createState() => _CardItemState();
}

class _CardItemState extends State<CardItem> with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> frontScale;
  Animation<double> backScale;
  String imagePrimary, imageSecondary;

  @override
  void initState() {
    super.initState();

    controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );

    frontScale = Tween(begin: 1.0, end: 0.0).animate(
      CurvedAnimation(
        parent: controller,
        curve: Interval(0.0, 0.5, curve: Curves.easeIn),
      ),
    );

    backScale = CurvedAnimation(
      parent: controller,
      curve: Interval(0.5, 1.0, curve: Curves.easeOut),
    );

    if (widget.model.status == ECardStatus.None) {
      imagePrimary = widget.model.image;
      imageSecondary = 'assets/00.jpg';
    } else {
      imagePrimary = 'assets/00.jpg';
      imageSecondary = widget.model.image;
    }

    if (widget.model.isNeedCloseEffect) {
      controller.reverse(from: 1.0);
      widget.model.isNeedCloseEffect = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Center(
        child: widget.model.status == ECardStatus.Win
            ? CardImage(image: widget.model.image)
            : Stack(
                children: <Widget>[
                  CardLayout(animation: backScale, image: imagePrimary),
                  CardLayout(animation: frontScale, image: imageSecondary),
                ],
              ),
      ),
      onTap: flipCard,
    );
  }


  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  void flipCard() {
    if (widget.model.status != ECardStatus.Win) {
      setState(() {
        if (controller.isCompleted || controller.velocity > 0) {
          controller
              .reverse()
              .then((v) => widget.onFlipCard(false, widget.model.id));
        } else {
          controller.forward().then((v) => widget.onFlipCard(
              widget.model.status == ECardStatus.None, widget.model.id));
        }
      });
    }
  }
  
}


