import 'package:flutter/material.dart';

enum ECardStatus { None, Win, Opened }

class CardModel {
  String image;
  int id;
  ECardStatus status;
  bool isNeedCloseEffect;
  Key key;

  CardModel({
    this.key,
    this.image,
    this.id,
    this.isNeedCloseEffect = false,
    this.status = ECardStatus.None,
  });
}
