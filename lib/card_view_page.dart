import 'dart:async';

import 'package:card_match/widgets/build_board.dart';
import 'package:card_match/widgets/build_score.dart';
import 'package:flutter/material.dart';

class CardViewPage extends StatefulWidget {
  @override
  _CardViewPageState createState() => _CardViewPageState();
}

class _CardViewPageState extends State<CardViewPage> {
  int score = 0;
  int time = 0;

  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 2), runTimer);
  }

  void runTimer() {
    Timer(Duration(seconds: 1), () {
      setState(() {
        this.time += 1;
        runTimer();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black87,
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Colors.black87,
              Colors.grey[800],
              Colors.black87,
            ],
          ),
        ),
        child: Column(
          children: <Widget>[
            SizedBox(height: 24.0),
            BuildScore(score: score, time: time,),
            BuildBoard(onWin: onWin),
          ],
        ),
      ),
    );
  }

  void onWin() {
    setState(() => this.score += 1);
  }
}
